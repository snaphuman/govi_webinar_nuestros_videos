<?php
/**
 * @file
 * govi_webinar_nuestros_videos.features.inc
 */

/**
 * Implements hook_node_info().
 */
function govi_webinar_nuestros_videos_node_info() {
  $items = array(
    'nuestros_videos' => array(
      'name' => t('Nuestros Videos'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Titulo del video'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
